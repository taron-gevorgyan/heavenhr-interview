Here is description how to you use service(api calls).

POST localhost:8080/api/offers/
	BODY should contain jobTitle and startDate. 
	F.e. {"jobTitle" : "Java Dev", "startDate" : "2017-01-15"}
	will create new offer

GET localhost:8080/api/offers/
	lists all offers

GET localhost:8080/api/offers/{id}
	will show offer with specified "id"

POST localhost:8080/api/offers/{offerId}/applications/
	BODY should contain email and resume text
	F.e. {"email" : "John@email.com", "resumeText" : "text"}
	will create new appliation for offer with "offerId"

POST localhost:8080/api/offers/{offerId}/applications/{applicationId}/status
	BODY should contain status
	F.e. {"status" : "APPLIED"}

GET localhost:8080/api/offers/{offerId}/applications/{applicationId}
	lists specified application for offer

GET localhost:8080/api/offers/{offerId}/applications/
	listst all applications for offer

GET localhost:8080/api/offers/applications/
	lists all applications


Task description https://github.com/HeavenHR/java-interview-exercise