package com.heavenhr.services;

import com.heavenhr.db.repositories.ApplicationRepository;
import com.heavenhr.db.repositories.OfferRepository;
import com.heavenhr.domain.Application;
import com.heavenhr.domain.ApplicationStatus;
import com.heavenhr.domain.Offer;
import com.heavenhr.services.impl.SimpleApplicationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceTest {

    @InjectMocks
    private SimpleApplicationService applicationService;

    @Mock
    private ApplicationRepository applicationRepository;

    @Mock
    private OfferRepository offerRepository;

    @Mock
    private ApplicationEventPublisher publisher;

    @Test
    public void testFindOfferApplications(){
        Offer offer = Offer.of("Java dev", LocalDate.now());
        ReflectionTestUtils.setField(offer, "id", 1L);
        Application application1 = Application.of(offer, "candidate1@email.com", "Resume Text");
        Application application2 = Application.of(offer, "candidate2@email.com", "Resume Text");
        List<Application> applicationList = new ArrayList<>();
        applicationList.add(application1);
        applicationList.add(application2);
        Mockito.when(applicationRepository.findByRelatedOffer(offer)).thenReturn(applicationList);
        Mockito.when(offerRepository.findOne(offer.getId())).thenReturn(offer);
        Collection<Application> readApplications = applicationService.findOfferApplications(offer.getId());

        List<Application> readApplicationsList = new ArrayList<>(readApplications);
        Assert.assertEquals(applicationList.size(), readApplicationsList .size());
        for(int i = 0; i < applicationList.size(); i++){
            Assert.assertEquals(applicationList.get(i), readApplicationsList.get(i));
        }
    }

    @Test
    public void testChangeStatus(){
        Offer offer = Offer.of("Java dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Resume Text");
        ReflectionTestUtils.setField(application, "id", 1L);
        Mockito.when(applicationRepository.findOne(application.getId())).thenReturn(application);
        applicationService.changeStatus(application.getId(), ApplicationStatus.HIRED);
        Application updatedApplication = applicationService.read(application.getId());
        Assert.assertEquals(ApplicationStatus.HIRED, updatedApplication.getStatus());
    }

    @Test
    public void testCreate() {
        Mockito.when(applicationService.create(any(Application.class))).thenAnswer(invocationOnMock -> {
            Application passApplication = invocationOnMock.getArgumentAt(0, Application.class);
            ReflectionTestUtils.setField(passApplication, "id", 1L);
            return passApplication;
        });

        Offer offer = Offer.of("Java dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Resume Text");
        Application createdApplication = applicationService.create(application);
        Mockito.verify(applicationRepository, Mockito.times(1)).save(application);

        Assert.assertEquals(application.getEmail(), createdApplication.getEmail());
        Assert.assertEquals(application.getResumeText(), createdApplication.getResumeText());
        Assert.assertEquals(application.getRelatedOffer(), createdApplication.getRelatedOffer());
        Assert.assertEquals(application.getStatus(), createdApplication.getStatus());
    }

    @Test
    public void testFindOne(){
        Offer offer = Offer.of("Java dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Resume Text");
        Mockito.when(applicationRepository.findOne(1L)).thenReturn(application);
        Application readApplication = applicationService.read(1L);

        Assert.assertEquals(application, readApplication);
    }

    @Test
    public void testFindAll(){
        Offer offer = Offer.of("Java dev", LocalDate.now());
        Application application1 = Application.of(offer, "candidate1@email.com", "Resume Text");
        Application application2 = Application.of(offer, "candidate2@email.com", "Resume Text");
        List<Application> applicationList = new ArrayList<>();
        applicationList.add(application1);
        applicationList.add(application2);
        Mockito.when(applicationRepository.findAll()).thenReturn(applicationList);
        Collection<Application> readApplications = applicationService.findAll();

        List<Application> readApplicationsList = new ArrayList<>(readApplications);
        Assert.assertEquals(applicationList.size(), readApplicationsList .size());
        for(int i = 0; i < applicationList.size(); i++){
            Assert.assertEquals(applicationList.get(i), readApplicationsList.get(i));
        }
    }
}
