package com.heavenhr.services;

import com.heavenhr.db.repositories.OfferRepository;
import com.heavenhr.domain.Offer;
import com.heavenhr.services.impl.SimpleOfferService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class OfferServiceTest {

    @InjectMocks
    private SimpleOfferService offerService;

    @Mock
    private OfferRepository offerRepository;

    @Test
    public void testCreate() {
        Mockito.when(offerService.create(any(Offer.class))).thenAnswer(invocationOnMock -> {
            Offer passedOffer = invocationOnMock.getArgumentAt(0, Offer.class);
            ReflectionTestUtils.setField(passedOffer, "id", 1L);
            return passedOffer;
        });

        Offer offer = Offer.of("Java dev", LocalDate.now());
        Offer savedOffer = offerService.create(offer);
        Mockito.verify(offerRepository, Mockito.times(1)).save(offer);

        Assert.assertEquals(offer.getJobTitle(), savedOffer.getJobTitle());
        Assert.assertEquals(offer.getStartDate(), savedOffer.getStartDate());
    }

    @Test
    public void testFindOne(){
        Offer offer = Offer.of("Java dev", LocalDate.now());
        Mockito.when(offerRepository.findOne(1L)).thenReturn(offer);
        Offer readOffer = offerService.read(1L);

        Assert.assertEquals(offer, readOffer);
    }

    @Test
    public void testFindAll(){
        Offer offer1 = Offer.of("Java dev", LocalDate.now());
        Offer offer2 = Offer.of("QA Engineer", LocalDate.now());
        List<Offer> offers = new ArrayList<>();
        offers.add(offer1);
        offers.add(offer2);
        Mockito.when(offerRepository.findAll()).thenReturn(offers);
        Collection<Offer> readOffers = offerService.findAll();

        List<Offer> readOffersList = new ArrayList<>(readOffers);
        Assert.assertEquals(offers.size(), readOffersList.size());
        for(int i = 0; i < offers.size(); i++){
            Assert.assertEquals(offers.get(i), readOffersList.get(i));
        }
    }
}
