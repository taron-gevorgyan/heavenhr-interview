package com.heavenhr.web.controllers;

import com.heavenhr.domain.Offer;
import com.heavenhr.services.OfferService;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(OfferController.class)
public class OfferControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OfferService offerService;

    @Captor
    private ArgumentCaptor<Offer> offerCaptor;

    @Test
    public void testFindOffer() throws Exception {
        Mockito.when(offerService.read(1))
                .thenReturn(Offer.of("Java Dev", LocalDate.now()));

        this.mvc.perform(get("/api/offers/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.jobTitle").value("Java Dev"))
                .andExpect(jsonPath("$.startDate").value(LocalDate.now().toString()));
    }

    @Test
    public void testFindAllOffers() throws Exception {
        Offer offer1 = Offer.of("Java Dev", LocalDate.now());
        Offer offer2 = Offer.of("QA Engineer", LocalDate.of(2020, 10, 20));
        ReflectionTestUtils.setField(offer1, "id", 1L);
        ReflectionTestUtils.setField(offer2, "id", 2L);
        List<Offer> offers = new ArrayList<>();
        offers.add(offer1);
        offers.add(offer2);
        Mockito.when(offerService.findAll()).thenReturn(offers);

        this.mvc.perform(get("/api/offers/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[0].jobTitle").value("Java Dev"))
                .andExpect(jsonPath("$[0].startDate").value(LocalDate.now().toString()))
                .andExpect(jsonPath("$[1].jobTitle").value("QA Engineer"))
                .andExpect(jsonPath("$[1].startDate").value(LocalDate.of(2020, 10, 20).toString()));
    }

    @Test
    public void testCreateOffer() throws Exception {
        String contentAsJson = "{\"jobTitle\":\"Java Dev\",\"startDate\":\"2018-10-01\"}";

        Mockito.when(offerService.create(offerCaptor.capture())).thenAnswer(invocationOnMock -> {
            Offer passedOffer = invocationOnMock.getArgumentAt(0, Offer.class);
            ReflectionTestUtils.setField(passedOffer, "id", 1L);
            return passedOffer;
        });

        this.mvc.perform(post("/api/offers/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentAsJson))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", CoreMatchers.endsWith("/api/offers/1")))
                .andDo(print());

        Offer value = offerCaptor.getValue();
        Assert.assertEquals("Java Dev", value.getJobTitle());
        Assert.assertEquals(LocalDate.of(2018, 10, 1), value.getStartDate());
    }
}
