package com.heavenhr.web.controllers;

import com.heavenhr.domain.Application;
import com.heavenhr.domain.ApplicationStatus;
import com.heavenhr.domain.Offer;
import com.heavenhr.services.ApplicationService;
import com.heavenhr.services.OfferService;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ApplicationController.class)
public class ApplicationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ApplicationService applicationService;

    @MockBean
    private OfferService offerService;

    @Captor
    private ArgumentCaptor<Application> applicationCaptor;

    @Captor
    private ArgumentCaptor<ApplicationStatus> statusCaptor;

    @Test
    public void testApply() throws Exception {
        String contentAsJson = "{\"email\":\"candidate1@email.com\",\"resumeText\":\"Text of candidate1\"}";

        Offer offer = Offer.of("Java dev", LocalDate.now());
        ReflectionTestUtils.setField(offer, "id", 1L);

        Mockito.when(applicationService.create(applicationCaptor.capture())).thenAnswer(invocationOnMock -> {
            Application passedApplication = invocationOnMock.getArgumentAt(0, Application.class);
            ReflectionTestUtils.setField(passedApplication, "id", 1L);
            return passedApplication;
        });
        Mockito.when(offerService.read(1)).thenReturn(offer);

        this.mvc.perform(post("/api/offers/1/applications/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentAsJson))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", CoreMatchers.endsWith("/api/offers/1/applications/1")))
                .andDo(print());
    }

    @Test
    public void testUpdateStatus() throws Exception{
        String contentAsJson = "{\"status\":\"HIRED\"}";

        this.mvc.perform(post("/api/offers/1/applications/1/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentAsJson))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void testReadApplication() throws Exception {
        Offer offer = Offer.of("Java dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Resume Text");
        ReflectionTestUtils.setField(application, "id", 1L);
        ReflectionTestUtils.setField(application, "status", ApplicationStatus.HIRED);
        Mockito.when(applicationService.read(1)).thenReturn(application);

        this.mvc.perform(get("/api/offers/1/applications/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.email").value("candidate1@email.com"))
                .andExpect(jsonPath("$.resumeText").value("Resume Text"))
                .andExpect(jsonPath("$.status").value(ApplicationStatus.HIRED.name()));
    }

    @Test
    public void testReadOfferApplications() throws Exception {
        Offer offer = Offer.of("Java dev", LocalDate.now());
        ReflectionTestUtils.setField(offer, "id", 1L);
        Application application1 = Application.of(offer, "candidate1@email.com", "Resume Text");
        Application application2 = Application.of(offer, "candidate2@email.com", "Resume Text");
        ReflectionTestUtils.setField(application1, "id", 1L);
        ReflectionTestUtils.setField(application2, "id", 2L);
        Set<Application> applications = new LinkedHashSet<>();
        applications.add(application1);
        applications.add(application2);
        ReflectionTestUtils.setField(offer, "applications", applications);
        Mockito.when(offerService.read(1)).thenReturn(offer);

        this.mvc.perform(get("/api/offers/1/applications/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].offerId").value(1))
                .andExpect(jsonPath("$[0].email").value("candidate1@email.com"))
                .andExpect(jsonPath("$[0].resumeText").value("Resume Text"))
                .andExpect(jsonPath("$[0].status").value(ApplicationStatus.APPLIED.name()))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].offerId").value(1))
                .andExpect(jsonPath("$[1].email").value("candidate2@email.com"))
                .andExpect(jsonPath("$[1].resumeText").value("Resume Text"))
                .andExpect(jsonPath("$[1].status").value(ApplicationStatus.APPLIED.name()));
    }

    @Test
    public void testFindAllApplications() throws Exception {
        Offer offer1 = Offer.of("Java dev", LocalDate.now());
        Offer offer2 = Offer.of("QA Engineer", LocalDate.now());
        ReflectionTestUtils.setField(offer1, "id", 1L);
        ReflectionTestUtils.setField(offer2, "id", 2L);
        Application application1 = Application.of(offer1, "candidate1@email.com", "Candidate1 for Java Dev");
        Application application2 = Application.of(offer1, "candidate2@email.com", "Candidate2 for Java Dev");
        Application application3 = Application.of(offer2, "candidate1@email.com", "Candidate1 for QA Engineer");
        Application application4 = Application.of(offer2, "candidate3@email.com", "Candidate3 for QA Engineer");
        ReflectionTestUtils.setField(application1, "id", 1L);
        ReflectionTestUtils.setField(application2, "id", 2L);
        ReflectionTestUtils.setField(application3, "id", 3L);
        ReflectionTestUtils.setField(application4, "id", 4L);
        Set<Application> applications = new LinkedHashSet<>();
        applications.add(application1);
        applications.add(application2);
        applications.add(application3);
        applications.add(application4);
        Mockito.when(applicationService.findAll()).thenReturn(applications);

        this.mvc.perform(get("/api/offers/applications/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].offerId").value(1))
                .andExpect(jsonPath("$[0].email").value("candidate1@email.com"))
                .andExpect(jsonPath("$[0].resumeText").value("Candidate1 for Java Dev"))
                .andExpect(jsonPath("$[0].status").value(ApplicationStatus.APPLIED.name()))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].offerId").value(1))
                .andExpect(jsonPath("$[1].email").value("candidate2@email.com"))
                .andExpect(jsonPath("$[1].resumeText").value("Candidate2 for Java Dev"))
                .andExpect(jsonPath("$[1].status").value(ApplicationStatus.APPLIED.name()))
                .andExpect(jsonPath("$[2].id").value(3))
                .andExpect(jsonPath("$[2].offerId").value(2))
                .andExpect(jsonPath("$[2].email").value("candidate1@email.com"))
                .andExpect(jsonPath("$[2].resumeText").value("Candidate1 for QA Engineer"))
                .andExpect(jsonPath("$[2].status").value(ApplicationStatus.APPLIED.name()))
                .andExpect(jsonPath("$[3].id").value(4))
                .andExpect(jsonPath("$[3].offerId").value(2))
                .andExpect(jsonPath("$[3].email").value("candidate3@email.com"))
                .andExpect(jsonPath("$[3].resumeText").value("Candidate3 for QA Engineer"))
                .andExpect(jsonPath("$[3].status").value(ApplicationStatus.APPLIED.name()));
    }
}
