package com.heavenhr.db.repositories;

import com.heavenhr.domain.Application;
import com.heavenhr.domain.ApplicationStatus;
import com.heavenhr.domain.Offer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ApplicationRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ApplicationRepository repository;

    @Test
    public void testFindByRelatedOffer() throws Exception{
        Offer offer = Offer.of("Java Dev", LocalDate.now());
        entityManager.persist(offer);
        entityManager.persist(Application.of(offer, "candidate1@email.com", "Some Text"));
        entityManager.persist(Application.of(offer, "candidate2@email.com", "Another Text"));

        Collection<Application> applications = repository.findByRelatedOffer(offer);
        List<Application> applicationList = new ArrayList<>(applications);
        assertThat(applicationList.size()).isEqualTo(2);

        assertThat(applicationList.get(0).getId()).isNotNull();
        assertThat(applicationList.get(0).getRelatedOffer()).isEqualTo(offer);
        assertThat(applicationList.get(0).getStatus()).isEqualTo(ApplicationStatus.APPLIED);
        assertThat(applicationList.get(0).getEmail()).isEqualTo("candidate1@email.com");
        assertThat(applicationList.get(0).getResumeText()).isEqualTo("Some Text");

        assertThat(applicationList.get(1).getId()).isNotNull();
        assertThat(applicationList.get(1).getRelatedOffer()).isEqualTo(offer);
        assertThat(applicationList.get(1).getStatus()).isEqualTo(ApplicationStatus.APPLIED);
        assertThat(applicationList.get(1).getEmail()).isEqualTo("candidate2@email.com");
        assertThat(applicationList.get(1).getResumeText()).isEqualTo("Another Text");
    }

    @Test
    public void testCountByRelatedOffer() throws Exception{
        Offer offer = Offer.of("Java Dev", LocalDate.now());
        entityManager.persist(offer);
        entityManager.persist(Application.of(offer, "candidate1@email.com", "Some Text"));
        entityManager.persist(Application.of(offer, "candidate2@email.com", "Another Text"));
        entityManager.persist(Application.of(offer, "candidate3@email.com", "Another Text"));

        int countByRelatedOffer = repository.countByRelatedOffer(offer);
        Assert.assertEquals(3, countByRelatedOffer);
    }

    @Test
    public void testFindAll() throws Exception {
        Offer offer = Offer.of("Java Dev", LocalDate.now());
        entityManager.persist(offer);
        entityManager.persist(Application.of(offer, "candidate1@email.com", "Some Text"));
        entityManager.persist(Application.of(offer, "candidate2@email.com", "Another Text"));

        List<Application> applicationList = repository.findAll();
        assertThat(applicationList.size()).isEqualTo(2);

        assertThat(applicationList.get(0).getId()).isNotNull();
        assertThat(applicationList.get(0).getRelatedOffer()).isEqualTo(offer);
        assertThat(applicationList.get(0).getStatus()).isEqualTo(ApplicationStatus.APPLIED);
        assertThat(applicationList.get(0).getEmail()).isEqualTo("candidate1@email.com");
        assertThat(applicationList.get(0).getResumeText()).isEqualTo("Some Text");

        assertThat(applicationList.get(1).getId()).isNotNull();
        assertThat(applicationList.get(1).getRelatedOffer()).isEqualTo(offer);
        assertThat(applicationList.get(1).getStatus()).isEqualTo(ApplicationStatus.APPLIED);
        assertThat(applicationList.get(1).getEmail()).isEqualTo("candidate2@email.com");
        assertThat(applicationList.get(1).getResumeText()).isEqualTo("Another Text");
    }

    @Test
    public void testFindOne() throws Exception {
        Offer offer = Offer.of("Java Dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Some Text");
        entityManager.persist(offer);
        entityManager.persist(application);

        Application readApplication = repository.findOne(application.getId());
        assertThat(readApplication).isNotNull();

        assertThat(readApplication.getId()).isNotNull();
        assertThat(readApplication.getRelatedOffer()).isEqualTo(offer);
        assertThat(readApplication.getStatus()).isEqualTo(ApplicationStatus.APPLIED);
        assertThat(readApplication.getEmail()).isEqualTo("candidate1@email.com");
        assertThat(readApplication.getResumeText()).isEqualTo("Some Text");
    }

    @Test
    public void testSave() throws Exception {
        Offer offer = Offer.of("Java Dev", LocalDate.now());
        Application application = Application.of(offer, "candidate1@email.com", "Some Text");
        entityManager.persist(offer);
        repository.save(application);

        Application readApplication = entityManager.find(Application.class, application.getId());
        assertThat(readApplication.getId()).isNotNull();
        assertThat(readApplication.getEmail()).isEqualTo("candidate1@email.com");
        assertThat(readApplication.getResumeText()).isEqualTo("Some Text");
        assertThat(readApplication.getRelatedOffer()).isEqualTo(offer);
        assertThat(readApplication.getStatus()).isEqualTo(ApplicationStatus.APPLIED);
    }
}
