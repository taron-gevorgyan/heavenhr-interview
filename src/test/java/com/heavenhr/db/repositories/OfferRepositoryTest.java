package com.heavenhr.db.repositories;

import com.heavenhr.domain.Offer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfferRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OfferRepository repository;

    @Test
    public void testFindAll() throws Exception {
        entityManager.persist(Offer.of("Java Dev", LocalDate.now()));
        entityManager.persist(Offer.of("QA Engineer", LocalDate.now()));

        List<Offer> offerList = repository.findAll();
        assertThat(offerList.size()).isEqualTo(2);
        assertThat(offerList.get(0).getId()).isNotNull();
        assertThat(offerList.get(0).getJobTitle()).isEqualTo("Java Dev");
        assertThat(offerList.get(0).getStartDate()).isEqualTo(LocalDate.now());
    }

    @Test
    public void testFindOne() throws Exception {
        Offer offer1 = Offer.of("Java Dev", LocalDate.now());
        Offer offer2 = Offer.of("QA Engineer", LocalDate.now());
        entityManager.persist(offer1);
        entityManager.persist(offer2);

        Offer offer = repository.findOne(offer2.getId());
        assertThat(offer.getId()).isNotNull();
        assertThat(offer.getJobTitle()).isEqualTo("QA Engineer");
        assertThat(offer.getStartDate()).isEqualTo(LocalDate.now());
    }

    @Test
    public void testSave() throws Exception {
        Offer savedOffer = repository.save(Offer.of("Java Dev", LocalDate.now()));

        Offer offer = entityManager.find(Offer.class, savedOffer.getId());
        assertThat(offer.getId()).isNotNull();
        assertThat(offer.getJobTitle()).isEqualTo("Java Dev");
        assertThat(offer.getStartDate()).isEqualTo(LocalDate.now());
    }
}
