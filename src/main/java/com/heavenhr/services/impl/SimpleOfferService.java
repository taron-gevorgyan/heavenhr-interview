package com.heavenhr.services.impl;

import com.heavenhr.db.repositories.OfferRepository;
import com.heavenhr.domain.Offer;
import com.heavenhr.services.OfferService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class SimpleOfferService implements OfferService {

    private final OfferRepository offerRepository;

    public SimpleOfferService(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @Transactional
    @Override
    public Offer create(Offer offer) {
        return offerRepository.save(offer);
    }

    @Override
    @Transactional(readOnly = true)
    public Offer read(long id) {
        return offerRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<Offer> findAll() {
        return offerRepository.findAll();
    }
}
