package com.heavenhr.services;

import com.heavenhr.domain.Offer;

import java.util.Collection;

public interface OfferService {

    Offer create(Offer offer);

    Offer read(long id);

    Collection<Offer> findAll();
}
