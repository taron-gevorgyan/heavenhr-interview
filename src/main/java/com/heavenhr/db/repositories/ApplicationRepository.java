package com.heavenhr.db.repositories;

import com.heavenhr.domain.Application;
import com.heavenhr.domain.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

    Collection<Application> findByRelatedOffer(Offer offer);

    int countByRelatedOffer(Offer offer);
}
