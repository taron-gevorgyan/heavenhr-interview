package com.heavenhr.domain;

public enum ApplicationStatus {
    APPLIED, INVITED, REJECTED, HIRED
}
