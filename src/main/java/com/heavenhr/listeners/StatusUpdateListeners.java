package com.heavenhr.listeners;

import com.heavenhr.domain.Application;
import com.heavenhr.domain.ApplicationStatus;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class StatusUpdateListeners {
    private Logger logger = Logger.getLogger(StatusUpdateListeners.class);

    @TransactionalEventListener(condition = "#application.status==T(com.heavenhr.domain.ApplicationStatus).APPLIED")
    public void listenAppliedEvent(Application application) {
        log(application.getId(), application.getStatus());
    }

    @TransactionalEventListener(condition = "#application.status==T(com.heavenhr.domain.ApplicationStatus).INVITED")
    public void listenInvitedEvent(Application application) {
        log(application.getId(), application.getStatus());
    }

    @TransactionalEventListener(condition = "#application.status==T(com.heavenhr.domain.ApplicationStatus).REJECTED")
    public void listenRejectedEvent(Application application) {
        log(application.getId(), application.getStatus());
    }

    @TransactionalEventListener(condition = "#application.status==T(com.heavenhr.domain.ApplicationStatus).HIRED")
    public void listenHiredEvent(Application application) {
        log(application.getId(), application.getStatus());
    }

    private void log(Long id, ApplicationStatus status){
        StringBuilder sb = new StringBuilder()
                .append("Application status for application ")
                .append(id)
                .append(" changed to ")
                .append(status.name());
        logger.info(sb.toString());
    }
}
